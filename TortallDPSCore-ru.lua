TortallDPSCoreRu = {}

-- Russian
ruParseTextTable = { dealt = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {} },
                      taken = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {} } }

--that's a crappy pattern, but dumbest way seems to be the rightest
--first match thinks that skillnames must only consist of following characters: alphanumeric, ".", "-", "?", "!" and space
--we can't use ".-" due to issues of translation
ruParseTextTable.dealt.damage.parse = L"^([%w%.%-%?, !]-) наносит цели %((.-)%) (%d+) ед%..-урона%."
--next crappy parrern relies on the sequence of matching. This one should be checked before taken.heals.parse
ruParseTextTable.dealt.heals.parse = L"^([%w%.%-%?, !]-) восполняет (%a-%(?.-%)?) (%d+) ед%. здоровья%."
ruParseTextTable.dealt.dodge.parse = L"(.-) завершается неудачно: цель %(.-%) уклоняется%."
ruParseTextTable.dealt.disrupt.parse = L"(.-) завершается неудачно: цель %(.-%) предотвращает срабатывание%."
ruParseTextTable.dealt.block.parse = L"(.-) завершается неудачно: цель %(.-%) ставит блок%."
ruParseTextTable.dealt.parry.parse = L"(.-) завершается неудачно: цель %(.-%) парирует%."

ruParseTextTable.dealt.damage.fields = { ability = 1, object = 2, amount = 3 }
ruParseTextTable.dealt.heals.fields = { ability = 1, object = 2, amount = 3 }
ruParseTextTable.dealt.block.fields = { ability = 1, object = 2 }
ruParseTextTable.dealt.disrupt.fields = { ability = 1, object = 2 }
ruParseTextTable.dealt.dodge.fields = { ability = 1, object = 2 }
ruParseTextTable.dealt.parry.fields = { ability = 1, object = 2 }

ruParseTextTable.taken.damage.parse = L"^(.-) %((.-)%) наносит вам (%d+) ед%..-урона%."
ruParseTextTable.taken.heals.parse = L"^(.-) %((.-)%) восполняет вам (%d+) ед%. здоровья%."

ruParseTextTable.taken.damage.fields = { ability = 1, object = 2, amount = 3 }
ruParseTextTable.taken.heals.fields = { ability = 1, object = 2, amount = 3 }

ruParseTextTable.mitigated = L"%(ослаблено: (%d+)%)"
ruParseTextTable.absorbed =  L"%(полглощено: (%d+)%)"
ruParseTextTable.dealt.crit = L"критическ"
ruParseTextTable.taken.crit = L"критическ"

local function russian_fixup( object, ability )
		object = object:gsub(L"цели ", "")
		object = object:gsub(L"%(", "")
		object = object:gsub(L"%)", "")
		
		ability = ability:gsub(L" критически", "")
		
		if object == L"вам" then
			object = GameData.Player.name
		end
		
		return object, ability
end

function TortallDPSCoreRu.InitLocale()
	ruParseTextTable.fixup = russian_fixup
	return ruParseTextTable
end