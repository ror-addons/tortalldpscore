LogDisplaySetEntryLimit("DebugWindowText", 5000)

TortallDPSCore = {}

TortallDPSCore.Addons = {}

TortallDPSCore.DAMAGE_DEALT = 1
TortallDPSCore.DAMAGE_TAKEN = 2
TortallDPSCore.HEAL_DEALT   = 3
TortallDPSCore.HEAL_TAKEN   = 4

TortallDPSCore.ABILITY = 1
TortallDPSCore.TARGET  = 2

local YOUR_HITS         = SystemData.ChatLogFilters.YOUR_HITS
local YOUR_HEALS        = SystemData.ChatLogFilters.YOUR_HEALS
local PET_DMG           = SystemData.ChatLogFilters.PET_DMG
local PET_HITS          = SystemData.ChatLogFilters.PET_HITS
local YOUR_DMG_FROM_PC  = SystemData.ChatLogFilters.YOUR_DMG_FROM_PC
local YOUR_DMG_FROM_NPC = SystemData.ChatLogFilters.YOUR_DMG_FROM_NPC

local SaveKey = L""

local ParseTextTable = {}

-- English
ParseTextTable[1] = { dealt = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {}, pet = {} },
                      taken = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {} } }

ParseTextTable[1].dealt.damage.parse = L"Your (.-) hits (.-)for (%d+) damage%."
ParseTextTable[1].dealt.pet.parse = L"Your (.-) hits (.-) for (%d+) damage%."
ParseTextTable[1].dealt.heals.parse = L"Your (.-) heals (.-) for (%d+) points%."
ParseTextTable[1].dealt.dodge.parse = L"(.+) dodged your (.-)%."
ParseTextTable[1].dealt.disrupt.parse = L"(.+) disrupted your (.-)%."
ParseTextTable[1].dealt.block.parse = L"(.+) blocked your (.-)%."
ParseTextTable[1].dealt.parry.parse = L"(.+) parried your (.-)%."

ParseTextTable[1].dealt.damage.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[1].dealt.pet.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[1].dealt.heals.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[1].dealt.block.fields = { ability = 2, object = 1 }
ParseTextTable[1].dealt.disrupt.fields = { ability = 2, object = 1 }
ParseTextTable[1].dealt.dodge.fields = { ability = 2, object = 1 }
ParseTextTable[1].dealt.parry.fields = { ability = 2, object = 1 }

ParseTextTable[1].taken.damage.parse = L"^(.-)'s (.-) hits you for (%d+) damage%."
ParseTextTable[1].taken.heals.parse = L"^(.-)'s (.-) heals you for (%d+) points%."

ParseTextTable[1].taken.damage.fields = { ability = 2, object = 1, amount = 3 }
ParseTextTable[1].taken.heals.fields = { ability = 2, object = 1, amount = 3 }

ParseTextTable[1].mitigated = L"%((%d+) mitigated%)"
ParseTextTable[1].absorbed =  L"%((%d+) absorbed%)"
ParseTextTable[1].dealt.crit = L"Your .- critically"
ParseTextTable[1].taken.crit = L".-'s .- critically"


-- French
ParseTextTable[2] = { dealt = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {}, pet = {} },
                      taken = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {} } }

-- Votre aptitude inflige un coup � Pouilleuh et provoque 33 points de d�g�ts. (7 att�nu�)
-- Votre Lingering Restore Essence vous administre un soin, et vous r�cup�rez 32 points de vie.
-- Votre Lingering Restore Essence administre un soin � Pafurtive, qui r�cup�re 210 points de vie.
-- Votre attaque inflige un coup critique � Silverymoon et provoque 0 points de d�g�ts. (12 att�nu�) (37 absorb�)
--Vous avez bloqu� l�attaque de Ritual.
ParseTextTable[2].dealt.damage.parse = L"Votre (.-) inflige un coup.- � (.-) et provoque (%d+) points de d�g�ts%."
ParseTextTable[2].dealt.pet.parse = L"Votre (.-) inflige un coup.- � (.-) et provoque (%d+) points de d�g�ts%."
ParseTextTable[2].dealt.heals.parse = L"Votre (.-) [vous ]*administre un soin.*[�t] (.-)[ ,qui]* r�cup[��]rez? (%d+) points de vie.+"
ParseTextTable[2].dealt.dodge.parse = L"(.+) a esquiv� votre (.-)%."
ParseTextTable[2].dealt.disrupt.parse = L"(.+) a dissip� votre (.-)%."
ParseTextTable[2].dealt.block.parse = L"(.+) a bloqu� votre (.-)%."
ParseTextTable[2].dealt.parry.parse = L"(.+) a par� votre (.-)%."

ParseTextTable[2].dealt.damage.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[2].dealt.pet.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[2].dealt.heals.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[2].dealt.block.fields = { ability = 2, object = 1 }
ParseTextTable[2].dealt.disrupt.fields = { ability = 2, object = 1 }
ParseTextTable[2].dealt.dodge.fields = { ability = 2, object = 1 }
ParseTextTable[2].dealt.parry.fields = { ability = 2, object = 1 }

-- le D�chirer l��me de Silverymoon vous inflige un coup et provoque 75 points de d�g�ts. (8 att�nu�)
-- la Col�re oppressante d�Yshen vous inflige un coup et provoque 109 points de d�g�ts. (32 att�nu�)
-- Le Aura chatoyante de Moige�l vous administre un soin critique�; vous r�cup�rez 240 points de vie.
-- l'attaque de Broyeur de pierres vous inflige un coup critique et provoque 0 points de d�g�ts.
ParseTextTable[2].taken.damage.parse = L"[Ll][ae%p][s%s]?(.+) d[e%p]%s*([A-Z].-) vous inflige un coup et provoque (%d+) points de d�g�ts.+"
ParseTextTable[2].taken.heals.parse = L"[Ll][ea%p]%s*(.+) d[%pe]%s*(.-) vous administre un soin.+ vous r�cup�rez (%d+) points de vie.+"

ParseTextTable[2].taken.damage.fields = { ability = 1, object = 2, amount = 3 }
ParseTextTable[2].taken.heals.fields = { ability = 1, object = 2, amount = 3 }

ParseTextTable[2].mitigated = L"%((%d+) att�nu�%)"
ParseTextTable[2].absorbed =  L"%((%d+) absorb�%)"
ParseTextTable[2].dealt.crit = L"critique"
ParseTextTable[2].taken.crit = L"critique"


-- German
ParseTextTable[3] = { dealt = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {}, pet = {} },
                      taken = { damage = {}, heals = {}, crit = {}, block = {}, disrupt = {}, dodge = {}, parry = {} } }

-- Ihr trefft das Festerweb Spider  und Euer Rumballan verursacht 273 Schadenspunkte. (Um 95 Punkte abgeschw�cht)
--Ihr trefft das Festerweb Spider kritisch und Euer Rumballan verursacht 409 Schadenspunkte. (Um 95 Punkte abgeschw�cht)
--Nitrid hat Euren Angriff geblockt.
-- Rakot trifft Euch, aber sein Verw�sten verursacht keinen Schaden. (423 Punkte wurden absorbiert)
-- Euer Begleiter trifft den Klippenr�cken-Keiler und sein Angriff verursacht 96 Schadenspunkte. (Um 39 Punkte abgeschw�cht)
ParseTextTable[3].dealt.damage.parse = L"Ihr trefft (.-) %s*und Eu[er]+ (.-) verursacht (%d+) Schadenspunkte%."
ParseTextTable[3].dealt.pet.parse = L"Euer Begleiter trifft (.-) und (.-) verursacht (%d+) Schadenspunkte%."
ParseTextTable[3].dealt.heals.parse = L"Ihr heilt (.-) %s*und Eu[er]+ (.-) stellt (%d+) Lebenspunkte wieder her%."
ParseTextTable[3].dealt.dodge.parse = L"(.+) dodged your (.-)%."
ParseTextTable[3].dealt.disrupt.parse = L"(.+) disrupted your (.-)%."
ParseTextTable[3].dealt.block.parse = L"(.+) hat Euren (.-) geblockt%."
ParseTextTable[3].dealt.parry.parse = L"(.+) parried your (.-)%."

ParseTextTable[3].dealt.damage.fields = { ability = 2, object = 1, amount = 3 }
ParseTextTable[3].dealt.pet.fields = { ability = 2, object = 1, amount = 3 }
ParseTextTable[3].dealt.heals.fields = { ability = 2, object = 1, amount = 3 }
ParseTextTable[3].dealt.block.fields = { ability = 2, object = 1 }
ParseTextTable[3].dealt.disrupt.fields = { ability = 2, object = 1 }
ParseTextTable[3].dealt.dodge.fields = { ability = 2, object = 1 }
ParseTextTable[3].dealt.parry.fields = { ability = 2, object = 1 }

-- Ein Spearman trifft Euch und sein Angriff verursacht 273 Schadenspunkte.
-- Gydane heilt Euch und ihr �Tzeentchs St�rkungsmittel� stellt 51 Lebenspunkte wieder her.
-- Gydane heilt Euch und ihre nachklingende dunkle Arznei stellt 203 Lebenspunkte wieder her.
ParseTextTable[3].taken.damage.parse = L"(.-) trifft Euch und %w+ (.-) %[*verursacht%]* (%d+) Schadenspunkte%."
ParseTextTable[3].taken.heals.parse = L"(.-) heilt Euch.*und %w+ (.-) %[*stellt%]* (%d+) Lebenspunkte wieder her%."

ParseTextTable[3].taken.damage.fields = { ability = 2, object = 1, amount = 3 }
ParseTextTable[3].taken.heals.fields = { ability = 2, object = 1, amount = 3 }

ParseTextTable[3].absorbed =  L"%((%d+) Punkte wurden absorbiert%)"
ParseTextTable[3].mitigated = L"%(Um (%d+) Punkte abgeschw�cht%)"
ParseTextTable[3].dealt.crit = L"Ihr .- kritisch und"
ParseTextTable[3].taken.crit = L"kritisch"

local ParseTable = nil


local function newstats()
    local stats = { paused = false, CombatTime = 0, save = true }

    stats.Damage = { Dealt = { Stats = {}, Object = {} }, Taken = { Stats = {}, Object = {} } }

    stats.Damage.Dealt.Total  = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0,
                                  Block = 0, Parry = 0, Disrupt = 0, Dodge = 0 }
    stats.Damage.Dealt.Normal = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }
    stats.Damage.Dealt.Crit   = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }

    stats.Damage.Taken.Total  = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0,
                                  Block = 0, Parry = 0, Disrupt = 0, Dodge = 0 }
    stats.Damage.Taken.Normal = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }
    stats.Damage.Taken.Crit   = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }


    stats.Healing = { Dealt = { Stats = {}, Object = {} }, Taken = { Stats = {}, Object = {} } }

    stats.Healing.Dealt.Total  = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0,
                                   Block = 0, Parry = 0, Disrupt = 0, Dodge = 0 }
    stats.Healing.Dealt.Normal = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }
    stats.Healing.Dealt.Crit   = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }

    stats.Healing.Taken.Total  = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0,
                                   Block = 0, Parry = 0, Disrupt = 0, Dodge = 0 }
    stats.Healing.Taken.Normal = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }
    stats.Healing.Taken.Crit   = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }

    return stats
end

local function english_fixup( object, ability )
    ability = ability:gsub(L" critically", "")
    ability = ability:gsub(L"^%[", "")
    ability = ability:gsub(L"%]$", "")

    if ( ability == L"attack" ) then ability = L"Auto Attack"      end
    if ( object == L"you" )     then object = GameData.Player.name end

    return object, ability
end

local function french_fixup( object, ability )
    ability = ability:gsub(L"^%[", "")
    ability = ability:gsub(L"%]$", "")

    if ( ability == L"attaque" ) then ability = L"Attaque auto"     end
    if ( object == L"vous" )     then object = GameData.Player.name end

    return object, ability
end

local function german_fixup( object, ability )
    ability = ability:gsub(L"^%[", "")
    ability = ability:gsub(L"%]$", "")
    ability = ability:gsub(L"sein ", L"Begleiter%-")
    ability = ability:gsub(L"seine ", L"Begleiter%-")

    object = object:gsub(L" kritisch", "")
    object = object:gsub(L"^das ", "")
    object = object:gsub(L"^den ", "")
    object = object:gsub(L"^die ", "")
    object = object:gsub(L"^Ein ", "")
    object = object:gsub(L"^Eine ", "")

    if ( object == L"Euch" ) then object = GameData.Player.name end

    return object, ability
end

function TortallDPSCore.Initialize()
    SaveKey = tostring(SystemData.Server.Name .. GameData.Player.name)
    if ( TortallDPSCore.SavedState == nil ) then
        TortallDPSCore.SavedState = { savestate = true, [SaveKey] = {} }
    end
    if ( TortallDPSCore.SavedState[SaveKey] == nil ) then TortallDPSCore.SavedState[SaveKey] = {} end

    ParseTextTable[1].fixup = english_fixup
    ParseTextTable[2].fixup = french_fixup
    ParseTextTable[3].fixup = german_fixup

    ParseTextTable[10] = TortallDPSCoreRu.InitLocale()

    ParseTable = ParseTextTable[SystemData.Settings.Language.active]

    TortallDPSCore.Stats = {}

    TortallDPSCore.Register("Global")

    RegisterEventHandler( SystemData.Events.UPDATE_PROCESSED, "TortallDPSCore.OnUpdate")
    RegisterEventHandler( TextLogGetUpdateEventId( "Combat" ), "TortallDPSCore.OnCombatLog" )
end

function TortallDPSCore.Shutdown()
end

function TortallDPSCore.Reset(channel)
    channel = tostring(channel) or "Global"

    TortallDPSCore.Stats[channel] = newstats()

    local addon
    for _, addon in pairs(TortallDPSCore.Addons) do
        if ( addon.Channels[channel] ~= nil ) then
            addon.Channels[channel] = TortallDPSCore.Stats[channel]
            if ( addon.update ~= nil ) and ( TortallDPSCore.Stats[channel].paused == false ) then
                local success, errmsg =
                    pcall(addon.update, addon.Channels[channel].Damage, addon.Channels[channel].Healing, addon.Channels[channel].CombatTime, channel)
                if ( success == false ) then
                    d(L"Error calling update callback")
                    d(towstring(errmsg))
                    -- Disable the callback that had an error.  Prevents spamming the debug log.
                    --addon.update = nil
                end
            end
        end
    end

    DEBUG(L"TortallDPSCore reset for "..towstring(channel))
end

function TortallDPSCore.Pause( channel, pause )
    if ( TortallDPSCore.Stats[channel] ~= nil ) then
        TortallDPSCore.Stats[channel].paused = pause
    end
end

function TortallDPSCore.Register( addon, update, newobject, latestobject, channel )
    if ( TortallDPSCore.Addons[addon] == nil ) then
        TortallDPSCore.Addons[addon] = {}
        TortallDPSCore.Addons[addon].Channels = {}
    end

    TortallDPSCore.Addons[addon].update = update
    TortallDPSCore.Addons[addon].newobject = newobject
    TortallDPSCore.Addons[addon].latestobject = latestobject

    if ( channel == nil ) then channel = "Global" end
    channel = tostring(channel)

    if ( TortallDPSCore.Stats[channel] == nil ) then
        if ( TortallDPSCore.SavedState.savestate == true ) and
           ( TortallDPSCore.SavedState[SaveKey][channel] ~= nil ) and ( TortallDPSCore.SavedState[SaveKey][channel].save == true ) then
            TortallDPSCore.Stats[channel] = TortallDPSCore.SavedState[SaveKey][channel]
        else
            TortallDPSCore.Stats[channel] = newstats()
            if ( TortallDPSCore.SavedState.savestate == true ) then
                TortallDPSCore.SavedState[SaveKey][channel] = TortallDPSCore.Stats[channel]
            end
        end
    end

    TortallDPSCore.Addons[addon].Channels[channel] = TortallDPSCore.Stats[channel]

    -- Notify the addon for any saved data
    if ( newobject ~= nil ) and ( TortallDPSCore.SavedState.savestate == true ) then
        local v
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Damage.Dealt.Object) do
            local success, errmsg = pcall(newobject, TortallDPSCore.DAMAGE_DEALT, v, TortallDPSCore.TARGET, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Damage.Dealt.Stats) do
            local success, errmsg = pcall(newobject, TortallDPSCore.DAMAGE_DEALT, v, TortallDPSCore.ABILITY, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Damage.Taken.Object) do
            local success, errmsg = pcall(newobject, TortallDPSCore.DAMAGE_TAKEN, v, TortallDPSCore.TARGET, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Damage.Taken.Stats) do
            local success, errmsg = pcall(newobject, TortallDPSCore.DAMAGE_TAKEN, v, TortallDPSCore.ABILITY, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end

        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Healing.Dealt.Object) do
            local success, errmsg = pcall(newobject, TortallDPSCore.HEALING_DEALT, v, TortallDPSCore.TARGET, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Healing.Dealt.Stats) do
            local success, errmsg = pcall(newobject, TortallDPSCore.HEALING_DEALT, v, TortallDPSCore.ABILITY, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Healing.Taken.Object) do
            local success, errmsg = pcall(newobject, TortallDPSCore.HEALING_TAKEN, v, TortallDPSCore.TARGET, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
        for _, v in pairs(TortallDPSCore.SavedState[SaveKey][channel].Healing.Taken.Stats) do
            local success, errmsg = pcall(newobject, TortallDPSCore.HEALING_TAKEN, v, TortallDPSCore.ABILITY, channel)
            if ( success == false ) then d(L"Error calling "..twostring(callback)..L" callback"); d(errmsg) end
        end
    end

    DEBUG(L"Registration from "..towstring(addon)..L" for channel "..towstring(channel)..L".")
end

function TortallDPSCore.OnUpdate( elapsed )
    if ( GameData.Player.inCombat ) then
        local v
        for _, v in pairs(TortallDPSCore.Stats) do
            if ( v.paused == false ) then v.CombatTime = v.CombatTime + elapsed end
        end
    end
end

function TortallDPSCore.OnCombatLog( updateType, filterType )
    if ( updateType ~= SystemData.TextLogUpdate.ADDED ) then return end

    local i, msg
    local num = TextLogGetNumEntries("Combat") - 1

    _, _, msg = TextLogGetEntry("Combat", num);
    TortallDPSCore.ParseEntry(msg, filterType)

    --d("filterType = " .. filterType) d(msg)

    local addon, channel
    for _, addon in pairs(TortallDPSCore.Addons) do
        for channel in pairs(addon.Channels) do
            if ( addon.update ~= nil ) and ( TortallDPSCore.Stats[channel].paused == false ) then
                local success, errmsg =
                    pcall(addon.update, addon.Channels[channel].Damage, addon.Channels[channel].Healing, addon.Channels[channel].CombatTime, channel)
                if ( success == false ) then
                    d(L"Error calling update callback")
                    d(towstring(errmsg))
                    -- Disable the callback that had an error.  Prevents spamming the debug log.
                    --addon.update = nil
                end
            end
        end
    end
end

local function newSubtable( name )
    local t = { Name = name }
    t.Amount = 0
    t.Mitigated = 0
    t.Absorbed = 0
    t.Count = 0
    t.Max = 0
    t.Min = nil
    t.Pet = false
    t.Block = 0
    t.Disrupt = 0
    t.Dodge = 0
    t.Parry = 0
    t.Normal = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }
    t.Crit   = { Amount = 0, Count = 0, Max = 0, Min = nil, Mitigated = 0, Absorbed = 0 }

    return t
end

local function notify( callback, dmgType, entry, objectType, channel, data )
    local addon
    for _, addon in pairs(TortallDPSCore.Addons) do
        if ( addon.Channels[channel] ~= nil ) and ( addon[callback] ~= nil ) then
            local success, errmsg = pcall(addon[callback], dmgType, entry, objectType, channel, data)
            if ( success == false ) then
                d(L"Error calling "..towstring(callback)..L" callback")
                d(towstring(errmsg))
                -- Disable the callback that had an error.  Prevents spamming the debug log.
                --addon[callback] = nil
            end
        end
    end
end

function TortallDPSCore.ParseEntry( text, filter )
    local matched
    local p_damage
    local d_damage, d_heal, d_block, d_disrupt, d_dodge, d_parry
    local t_damage, t_heal, t_block, t_disrupt, t_dodge, t_parry
    local fields = { p_damage = {},
                     d_damage = {}, d_heal = {}, d_block = {}, d_disrupt = {}, d_dodge = {}, d_parry = {},
                     t_damage = {}, t_heal = {}, t_block = {}, t_disrupt = {}, t_dodge = {}, t_parry = {} }

    if ( filter == PET_HITS ) then
                            p_damage,  _, fields.p_damage[1],  fields.p_damage[2],  fields.p_damage[3]  = text:find(ParseTable.dealt.pet.parse)
                            matched = ( p_damage ~= nil ) end
    if ( not matched ) then d_damage,  _, fields.d_damage[1],  fields.d_damage[2],  fields.d_damage[3]  = text:find(ParseTable.dealt.damage.parse)
                            matched = ( d_damage ~= nil ) end
    if ( not matched ) then d_heal,    _, fields.d_heal[1],    fields.d_heal[2],    fields.d_heal[3]    = text:find(ParseTable.dealt.heals.parse)
                            matched = ( d_heal ~= nil ) end
    if ( not matched ) then d_block,   _, fields.d_block[1],   fields.d_block[2],   fields.d_block[3]   = text:find(ParseTable.dealt.block.parse)
                            matched = ( d_block ~= nil ) end
    if ( not matched ) then d_disrupt, _, fields.d_disrupt[1], fields.d_disrupt[2], fields.d_disrupt[3] = text:find(ParseTable.dealt.disrupt.parse)
                            matched = ( d_disrupt ~= nil ) end
    if ( not matched ) then d_dodge,   _, fields.d_dodge[1],   fields.d_dodge[2],   fields.d_dodge[3]   = text:find(ParseTable.dealt.dodge.parse)
                            matched = ( d_dodge ~= nil ) end
    if ( not matched ) then d_parry,   _, fields.d_parry[1],   fields.d_parry[2],   fields.d_parry[3]   = text:find(ParseTable.dealt.parry.parse)
                            matched = ( d_parry ~= nil ) end

    if ( not matched ) then t_damage,  _, fields.t_damage[1],  fields.t_damage[2],  fields.t_damage[3]  = text:find(ParseTable.taken.damage.parse)
                        matched = ( t_damage ~= nil ) end
    if ( not matched ) then t_heal,    _, fields.t_heal[1],    fields.t_heal[2],    fields.t_heal[3]    = text:find(ParseTable.taken.heals.parse)
                            matched = ( t_heal ~= nil ) end

    if ( not matched ) then return end

    local ability, object
    local amount, mitigated, absorbed = 0, 0, 0
    local critical

    if ( p_damage ) then
        dmgType = TortallDPSCore.DAMAGE_DEALT
        ability = fields.p_damage[ParseTable.dealt.damage.fields.ability]
        object  = fields.p_damage[ParseTable.dealt.damage.fields.object]
        amount  = fields.p_damage[ParseTable.dealt.damage.fields.amount]

        mitigated = text:match(ParseTable.mitigated)
        if ( mitigated == nil ) then mitigated = 0 end

        absorbed = text:match(ParseTable.absorbed)
        if ( absorbed == nil ) then absorbed = 0 end

        critical = text:match(ParseTable.dealt.crit)
    end

    if ( d_damage ) then
        dmgType = TortallDPSCore.DAMAGE_DEALT
        ability = fields.d_damage[ParseTable.dealt.damage.fields.ability]
        object  = fields.d_damage[ParseTable.dealt.damage.fields.object]
        amount  = fields.d_damage[ParseTable.dealt.damage.fields.amount]

        mitigated = text:match(ParseTable.mitigated)
        if ( mitigated == nil ) then mitigated = 0 end

        absorbed = text:match(ParseTable.absorbed)
        if ( absorbed == nil ) then absorbed = 0 end

        critical = text:match(ParseTable.dealt.crit)
    end

    if ( d_heal ) then
        dmgType = TortallDPSCore.HEAL_DEALT
        ability = fields.d_heal[ParseTable.dealt.heals.fields.ability]
        object  = fields.d_heal[ParseTable.dealt.heals.fields.object]
        amount  = fields.d_heal[ParseTable.dealt.heals.fields.amount]

        critical = text:match(ParseTable.dealt.crit)
    end

    if ( t_damage ) then
        dmgType = TortallDPSCore.DAMAGE_TAKEN
        ability = fields.t_damage[ParseTable.taken.damage.fields.ability]
        object  = fields.t_damage[ParseTable.taken.damage.fields.object]
        amount  = fields.t_damage[ParseTable.taken.damage.fields.amount]

        mitigated = text:match(ParseTable.mitigated)
        if ( mitigated == nil ) then mitigated = 0 end

        absorbed = text:match(ParseTable.absorbed)
        if ( absorbed == nil ) then absorbed = 0 end

        critical = text:match(ParseTable.taken.crit)
    end

    if ( t_heal ) then
        dmgType = TortallDPSCore.HEAL_TAKEN
        ability = fields.t_heal[ParseTable.taken.heals.fields.ability]
        object  = fields.t_heal[ParseTable.taken.heals.fields.object]
        amount  = fields.t_heal[ParseTable.taken.heals.fields.amount]

        critical = text:match(ParseTable.taken.crit)
    end

    if ( d_block ) then
        dmgType = TortallDPSCore.DAMAGE_DEALT
        ability = fields.d_block[ParseTable.dealt.block.fields.ability]
        object  = fields.d_block[ParseTable.dealt.block.fields.object]
    end

    if ( d_disrupt ) then
        dmgType = TortallDPSCore.DAMAGE_DEALT
        ability = fields.d_disrupt[ParseTable.dealt.disrupt.fields.ability]
        object  = fields.d_disrupt[ParseTable.dealt.disrupt.fields.object]
    end

    if ( d_dodge ) then
        dmgType = TortallDPSCore.DAMAGE_DEALT
        ability = fields.d_dodge[ParseTable.dealt.dodge.fields.ability]
        object  = fields.d_dodge[ParseTable.dealt.dodge.fields.object]
    end

    if ( d_parry ) then
        dmgType = TortallDPSCore.DAMAGE_DEALT
        ability = fields.d_parry[ParseTable.dealt.parry.fields.ability]
        object  = fields.d_parry[ParseTable.dealt.parry.fields.object]
    end

    if ( object == nil ) or ( ability == nil ) then
        d(text)
        return
    end

    if ( d_damage ) and ( object == GameData.Player.name:match(L"([^^]+)^?.*") ) then
        return
    end

    object, ability = ParseTable.fixup(object, ability)

    amount = tonumber(amount)
    mitigated = tonumber(mitigated)
    absorbed = tonumber(absorbed)

    local subTable

    local data = newSubtable()
    data.Count = 1
    data.Amount = amount
    data.Mitigated = mitigated
    data.Absorbed = absorbed
    data.Ability = ability
    data.Object = object
    data.Pet = ( filter == PET_HITS )
    if ( d_block )   then data.Block   = 1 end
    if ( d_disrupt ) then data.Disrupt = 1 end
    if ( d_dodge )   then data.Dodge   = 1 end
    if ( d_parry )   then data.Parry   = 1 end
    data.Critical = ( critical ~= nil )

    local channel, stats
    for channel, stats in pairs(TortallDPSCore.Stats) do
        if ( stats.paused == false ) then
            if ( p_damage )  then subTable = stats.Damage.Dealt end
            if ( d_damage )  then subTable = stats.Damage.Dealt end
            if ( d_block )   then subTable = stats.Damage.Dealt end
            if ( d_disrupt ) then subTable = stats.Damage.Dealt end
            if ( d_dodge )   then subTable = stats.Damage.Dealt end
            if ( d_parry )   then subTable = stats.Damage.Dealt end
            if ( d_heal )    then subTable = stats.Healing.Dealt end
            if ( t_damage )  then subTable = stats.Damage.Taken end
            if ( t_heal )    then subTable = stats.Healing.Taken end

            subTable.Total.Amount    = subTable.Total.Amount + data.Amount
            subTable.Total.Mitigated = subTable.Total.Mitigated + data.Mitigated
            subTable.Total.Absorbed  = subTable.Total.Absorbed + data.Absorbed
            subTable.Total.Count     = subTable.Total.Count + data.Count
            subTable.Total.Block     = subTable.Total.Block + data.Block
            subTable.Total.Disrupt   = subTable.Total.Disrupt + data.Disrupt
            subTable.Total.Dodge     = subTable.Total.Dodge + data.Dodge
            subTable.Total.Parry     = subTable.Total.Parry + data.Parry
            if ( subTable.Total.Min == nil ) or ( subTable.Total.Min > data.Amount ) then subTable.Total.Min = data.Amount end
            if ( subTable.Total.Max < data.Amount ) then subTable.Total.Max = data.Amount end

            if ( subTable.Stats[ability] == nil ) then
                subTable.Stats[ability] = newSubtable(ability)
                notify("newobject", dmgType, subTable.Stats[ability], TortallDPSCore.ABILITY, channel, data)
            end

            if ( subTable.Object[object] == nil ) then
                subTable.Object[object] = newSubtable(object)
                notify("newobject", dmgType, subTable.Object[object], TortallDPSCore.TARGET, channel, data)
            end

            TortallDPSCore.UpdateEntry(subTable.Stats[ability], data)
            TortallDPSCore.UpdateEntry(subTable.Object[object], data)

            notify("latestobject", dmgType, subTable.Stats[ability], TortallDPSCore.ABILITY, channel, data)
            notify("latestobject", dmgType, subTable.Object[object], TortallDPSCore.TARGET, channel, data)

            if ( data.Critical == false ) then
                subTable = subTable.Normal
            else
                subTable = subTable.Crit
            end

            subTable.Amount    = subTable.Amount + data.Amount
            subTable.Mitigated = subTable.Mitigated + data.Mitigated
            subTable.Absorbed  = subTable.Absorbed + data.Absorbed
            subTable.Count     = subTable.Count + data.Count
            if ( subTable.Min == nil ) or ( subTable.Min > data.Amount ) then subTable.Min = data.Amount end
            if ( subTable.Max < data.Amount ) then subTable.Max = data.Amount end
        end
    end
end

function TortallDPSCore.UpdateEntry(entry, data)
    entry.Amount    = entry.Amount + data.Amount
    entry.Mitigated = entry.Mitigated + data.Mitigated
    entry.Absorbed  = entry.Absorbed + data.Absorbed
    entry.Count     = entry.Count + data.Count
    entry.Block     = entry.Block + data.Block
    entry.Disrupt   = entry.Disrupt + data.Disrupt
    entry.Dodge     = entry.Dodge + data.Dodge
    entry.Parry     = entry.Parry + data.Parry
    if ( entry.Min == nil ) or ( entry.Min > data.Amount ) then entry.Min = data.Amount end
    if ( entry.Max < data.Amount ) then entry.Max = data.Amount end

    local subTable
    if ( data.Critical == false ) then
        subTable = entry.Normal
    else
        subTable = entry.Crit
    end

    subTable.Amount    = subTable.Amount + data.Amount
    subTable.Mitigated = subTable.Mitigated + data.Mitigated
    subTable.Absorbed  = subTable.Absorbed + data.Absorbed
    subTable.Count     = subTable.Count + data.Count
    if ( subTable.Min == nil ) or ( subTable.Min > data.Amount ) then subTable.Min = data.Amount end
    if ( subTable.Max < data.Amount ) then subTable.Max = data.Amount end
end

function TortallDPSCore.Save( save, channel )
    if ( channel ~= nil ) then
        TortallDPSCore.SavedState[SaveKey][channel].save = save
    else
        TortallDPSCore.SavedState.savestate = save
    end
end
